#include "algorithm.h"

const double TURN_OVER = M_PI * 2;
const double TURN_LEFT = TURN_OVER / 4;
const double TURN_RIGHT = -TURN_LEFT;

const double DEFAULT_LINEAR = 0.15;
const double SPRINT_LINEAR = 0.4;

#define is_black(color) (color < threshold)


geometry_msgs::Twist control(
	std::unordered_map<std::string, double> proximity,
	std::unordered_map<std::string, double> reflectance,
	double threshold
) {
	geometry_msgs::Twist msg;
	double left = reflectance["left"],
		center = reflectance["center"],
		right = reflectance["right"];

	std::cerr << "left = " << left
		<< ", center = " << center
		<< ", right = " << right
		<< ", threshold = " << threshold << '\n';

	std::vector<bool> sensor{ is_black(left), is_black(center), is_black(right) };

	// all on black
	if (sensor == std::vector < bool > {1, 1, 1}) {
		std::cerr << "all on black\n";
		msg.linear.x = SPRINT_LINEAR;
	}

	//
	else if (sensor == std::vector < bool > {1, 1, 0}) {
		std::cerr << "right on white turn left\n";
		msg.linear.x = DEFAULT_LINEAR / 3;
		msg.angular.z = TURN_LEFT / 2;
	}
	else if (sensor == std::vector < bool > {0, 1, 1}) {
		std::cerr << "left on white turn right\n";
		msg.linear.x = DEFAULT_LINEAR / 3;
		msg.angular.z = TURN_RIGHT / 2;
	}
	else if (sensor == std::vector < bool > {0, 1, 0}) {
		std::cerr << "front on white turn right\n";
		msg.linear.x = -DEFAULT_LINEAR / 5;
		msg.angular.z = TURN_RIGHT;
	}


	else if (sensor == std::vector < bool > {1, 0, 1}) {
		std::cerr << "front on black speed up\n";
		msg.linear.x = 1.5 * DEFAULT_LINEAR;
	}
	else if (sensor == std::vector < bool > {1, 0, 0}) {
		std::cerr << "left on black turn left\n";
		msg.linear.x = DEFAULT_LINEAR;
		msg.angular.z = TURN_LEFT;
	}
	else if (sensor == std::vector < bool > {0, 0, 1}) {
		std::cerr << "right on black turn right\n";
		msg.linear.x = DEFAULT_LINEAR;
		msg.angular.z = TURN_RIGHT;
	}
	else if (sensor == std::vector < bool > {0, 0, 0}) {
		std::cerr << "all on white\n";
		msg.linear.x = - DEFAULT_LINEAR;
	}


	return msg;
}
